# readme images

Collection of images to embed into the readmes of other projects of mine, but I don't want people who clone my repo to have to download these example images.

This is essentially just a way to host those images externally. 
